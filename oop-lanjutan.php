<?php

trait Hewan
{
    public $nama, $darah = 50, $jumlahKaki, $keahlian ;

    public function atraksi()
    {
        return "{$this->nama} sedang {$this->keahlian}";
    }
}


trait Fight
{
    public $attackPower, $defencePower ;

    public function serang($diserang)
    {
        echo "{$this->nama} sedang menyerang {$diserang} <br>";
        $this->diserang($diserang);
        echo "</br>";

    }

    public function diserang($diserang)
    {
        $daraAkhir = $this->darah - $this->attackPower/$this->defencePower;
        echo "{$diserang} sedang diserang <br> darah sekarang tersisa: $daraAkhir";
    }
}


class Elang
{
    use Hewan, Fight;
    public $jenis = "Elang";
    public function getInfoHewan(){
        return "Nama hewan adalah {$this->nama}, banyak darah {$this->darah}, memiliki kaki {$this->jumlahKaki}, dengan kemampuan menyerang {$this->attackPower}, dan pertahanan {$this->defencePower}, jenis hewan adalah {$this->jenis}";
    }

}

class Harimau
{
    use Hewan, Fight;
    public $jenis = "Harimau";

    public function getInfoHewan(){
        return "Nama hewan adalah {$this->nama}, banyak darah {$this->darah}, memiliki kaki {$this->jumlahKaki}, dengan kemampuan menyerang {$this->attackPower}, dan pertahanan {$this->defencePower}, jenis hewan adalah {$this->jenis}";
    }
}

// Ketika Elang diinstansiasi, maka jumlahKaki bernilai 2, dan keahlian bernilai “terbang tinggi”, attackPower = 10 , deffencePower = 5 ;
$elang1 = new Elang();
$elang1->nama = "Elang_1";
$elang1->jumlahKaki = 2;
$elang1->keahlian = "terbang tinggi";
$elang1->attackPower = 10;
$elang1->defencePower = 5;

$elang2 = new Elang();
$elang2->nama = "Elang_2";
$elang2->jumlahKaki = 2;
$elang2->keahlian = "terbang tinggi";
$elang2->attackPower = 10;
$elang2->defencePower = 5;
// Ketika Harimau diintansiasi, maka jumlahKaki bernilai 4, dan keahlian bernilai “lari cepat” , attackPower = 7 , deffencePower = 8 ;
$elang3 = new Elang();
$elang3->nama = "Elang_3";
$elang3->jumlahKaki = 2;
$elang3->keahlian = "terbang tinggi";
$elang3->attackPower = 10;
$elang3->defencePower = 5;

$harimau1 = new Harimau();
$harimau1->nama = "Harimau_1";
$harimau1->jumlahKaki = 4;
$harimau1->keahlian = "lari cepat";
$harimau1->attackPower = 7;
$harimau1->defencePower = 8;

$harimau2 = new Harimau();
$harimau2->nama = "Harimau_2";
$harimau2->jumlahKaki = 4;
$harimau2->keahlian = "lari cepat";
$harimau2->attackPower = 7;
$harimau2->defencePower = 8;

$harimau3 = new Harimau();
$harimau3->nama = "Harimau_3";
$harimau3->jumlahKaki = 4;
$harimau3->keahlian = "lari cepat";
$harimau3->attackPower = 7;
$harimau3->defencePower = 8;

echo $elang1->atraksi();
echo "<br>";
echo $elang2->atraksi();
echo "<br>";
echo $elang3->atraksi();
echo "<br>";
echo $elang1->getInfoHewan();
echo "<br>";
echo $elang2->getInfoHewan();
echo "<br>";
echo $elang3->getInfoHewan();
echo "<br>";
echo "<br>";
echo $harimau1->atraksi();
echo "<br>";
echo $harimau2->atraksi();
echo "<br>";
echo $harimau3->atraksi();
echo "<br>";
echo $harimau1->getInfoHewan();
echo "<br>";
echo $harimau2->getInfoHewan();
echo "<br>";
echo $harimau3->getInfoHewan();
echo "<br>";
echo "<br>";
echo $harimau1->serang($elang3->nama);
echo "<br>";
echo $harimau2->serang($elang1->nama);
echo "<br>";
echo $elang3->serang($harimau3->nama);
echo "<br>";
